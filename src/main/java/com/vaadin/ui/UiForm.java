package com.vaadin.ui;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.polymertemplate.Id;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.templatemodel.TemplateModel;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.dependency.HtmlImport;
import com.vaadin.flow.component.polymertemplate.PolymerTemplate;

/**
 * A Designer generated component for the ui-form template.
 *
 * Designer will add and remove fields with @Id mappings but
 * does not overwrite or otherwise change this file.
 */
@Tag("ui-form")
@HtmlImport("ui-form.html")
public class UiForm extends PolymerTemplate<UiForm.UiFormModel> {



    public UiForm() {
        // You can initialise any data required for the connected UI components here.
    }

    /**
     * This model binds properties between UiForm and ui-form
     */
    public interface UiFormModel extends TemplateModel {
        // Add setters and getters for template properties here.
    }
}
